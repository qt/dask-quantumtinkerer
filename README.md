# dask-quanutminkerer #

This is a workflow-specific package for using `dask-jobqueue` in [Quantum Tinkerer
](https://quantumtinkerer.group) group.


## How to use it

To start using it, we first import the `Cluster` object:

```python
from dask_quantumtinkerer import Cluster

cluster = Cluster(nodes, adapt_min=None, extra_path=None, asynchronous=False)
```

where `nodes` is the number nodes you wish to use, `adapt_min` is a keyword
argument that initiates adaptive scaling and specifies the minimum
number of nodes the cluster can run with (for example, `adapt_min=2`) and
`extra_path` is the `PYTHONPATH` you wish to pass in order to use imports on
hpc05 side. If you wish to run the cluster asynchronously, you can also
do it via `asynchronous=True`.

We then launch the cluster itself:

```python
cluster.launch_cluster()
```

this will take a while as it ensures no version mismatches, forwards the
ports and launches the cluster.

After that, you should see a link printed which is your dashboard address.
In order to start calculating, we call the client:

```python
client = cluster.get_client()
```

where it also supports asynchronous clients (if so, remember to `await` it). From this point forward,
you can proceed with the usual `client` syntax such as `.map`, `.submit`,
`.scatter`, etc.

After you are done, it is highly advisable to close the client and the
cluster:

```python
client.shutdown()
```


## How it works ##
Our cluster has single-user ssh access. Also, it has awesome
[Singularity](https://sylabs.io/singularity/) installed. Also, we use
Jupyterhub. Here is what we do with these ingredients.

[research-docker](https://gitlab.kwant-project.org/qt/research-docker) CI
pipeline builds equivalent Docker and Singularity images.
During the build, URL for downloading Singularity container is stored in file
in the env variable `SINGULARITY_URL`.

Whenever the cluster is launched through the `Cluster.launch_cluster()` command,
it first compares the `SINGULARITY_URL` in the `research-docker.simg` container on
hpc05 and on io.
If the image doesn't exist or there is a version mismatch, we download it on hpc05.

Then the code forwards two random ports and launches the cluster through the
`dask-quantumtinkerer-server` command on hpc05.


## License ##

This package is distributed under MIT license.
