import asyncio
import os
import sys
import time
from threading import Condition, Thread
from random import randint
from warnings import warn

import asyncssh
from distributed import Client
from traitlets import Bool, Dict, Int, default
from traitlets.config.configurable import Configurable

from dask_quantumtinkerer import prepare


def dashboard_url(port):
    if "JUPYTERHUB_HOST" not in os.environ:
        return f"http://localhost:{port+1}/status"
    return f"{os.environ['JUPYTERHUB_HOST']}/user/{os.environ.get('JUPYTERHUB_USER')}/proxy/{port+1}/status"


class Cluster(Configurable):
    nodes = Int(help="number of nodes to scale")
    adapt_min = Int(
        help="Minimum number of nodes to keep with adaptive scaling", allow_none=True
    )
    port = Int(help="Port to use")
    kwargs = Dict(help="Additional Arguments to be passed to dask PBS cluster")
    asynchronous = Bool(help="run cluster asynchronously")

    @default("port")
    def _default_port(self):
        return randint(10000, 65535)

    def _launch_server_cmd(self, manifest_path):
        return (
            f"~/.pixi/bin/pixi run --manifest-path {manifest_path} dask-quantumtinkerer-server "
            f"--nodes={self.nodes} "
            f"--adapt_min={self.adapt_min} "
            f"--asynchronous={self.asynchronous} "
            f"--port={self.port}"
        ) + " ".join([f"--kwargs {key}={value}" for key, value in self.kwargs.items()])

    async def start_cluster(self, condition, manifest_path) -> None:
        async with asyncssh.connect("hpc05", known_hosts=prepare.known_hosts()) as conn:
            try:
                print("Forwarding ports...")
                await conn.forward_local_port("", self.port, "localhost", self.port)
                await conn.forward_local_port(
                    "", self.port + 1, "localhost", self.port + 1
                )
                print("Ports forwarded. Launching cluster")
                with condition:
                    condition.notify()
                msg = await conn.run(self._launch_server_cmd(manifest_path), check=True)
                print(f"cluster shut down with output:\n\n {msg}.")
            except asyncssh.ProcessError as e:
                with condition:
                    condition.notify()
                print(
                    f"unexpected error with output:\n\n {e.stdout}\n\n and error:\n\n {e.stderr}"
                )
                raise e

    async def _launch_cluster(self, condition):
        await prepare.ensure_pixi_git("hpc05")
        manifest_path = await prepare.sync_repo("hpc05")
        await self.start_cluster(condition, manifest_path)

    def launch_cluster(self, ignore_uncommitted=False):
        """Launch a dask cluster on the remote machine.

        Parameters
        ----------
        ignore_uncommitted : bool
            If True, ignore uncommitted changes in the package directory and pixi environment.
        """
        modified_files = prepare.uncommitted_package_files()
        if modified_files and not ignore_uncommitted:
            raise ValueError(
                f"Uncommitted changes in package files: {modified_files}. "
                "Please commit or stash these changes before launching the cluster."
            )
        elif modified_files:
            warn(
                f"Warning: Uncommitted changes in package files: {modified_files}. "
                "Ignoring these changes and launching the cluster.",
                UserWarning,
            )

        def _launch_in_thread(condition):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            loop.run_until_complete(self._launch_cluster(condition))
            loop.close()

        try:
            condition = Condition()
            with condition:
                thread = Thread(target=_launch_in_thread, args=(condition,))
                thread.start()
                condition.wait()
                time.sleep(10)  # give time for cluster to launch
                print(f"Dashboard URL: {dashboard_url(self.port)}")
        except Exception as e:
            with condition:
                condition.notify()
            raise (e)
            sys.exit(1)

    def get_client(self):
        return Client(f"localhost:{self.port}", asynchronous=self.asynchronous)
