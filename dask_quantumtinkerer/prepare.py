from pathlib import Path
from tomllib import load

import asyncssh
import git


def known_hosts():
    """Read known hosts from the default locations."""
    return asyncssh.read_known_hosts(
        [
            file
            for file in ("~/.ssh/known_hosts", "/etc/ssh/ssh_known_hosts")
            if Path(file).expanduser().exists()
        ]
    )


async def ensure_pixi_git(host, conn=None):
    """Check that pixi is available on the remote machine and install git with it."""
    if conn is None:
        conn = await asyncssh.connect(host, known_hosts=known_hosts())
        close_conn = True
    else:
        close_conn = False

    try:
        await conn.run(
            "test -e .pixi/bin/pixi || curl -fsSL https://pixi.sh/install.sh | bash;"
            "pixi_version=$(.pixi/bin/pixi --version) && "
            "required_version=0.42 && "
            'if [ $(echo -e "$pixi_version\n$required_version" | sort -V | head -n1) != "$required_version" ]; then '
            ".pixi/bin/pixi --self-update; "
            "fi &&"
            " test -e .pixi/bin/git || .pixi/bin/pixi global install -q git",
            check=True,
        )
    finally:
        if close_conn:
            conn.close()


def uncommitted_package_files():
    """Return package files that have uncommitted changes."""
    repo = git.Repo(".", search_parent_directories=True)
    all_changed = {item.a_path for item in repo.index.diff(None)}
    changed_config = all_changed & {"pyproject.toml", "pixi.toml", "pixi.lock"}

    # Check for uncommitted changes in the package directory
    try:
        with open("pyproject.toml", "rb") as f:
            pyproject_data = load(f)
    except FileNotFoundError:
        return changed_config

    package_name = pyproject_data["project"]["name"]
    # We assume that the package directory is the same as the package name.
    # This is not guaranteed, but it is a start.
    package_dir = package_name.replace("-", "_")

    changed_package_files = {
        item for item in all_changed if item.startswith(package_dir)
    }

    return changed_package_files | changed_config


async def sync_repo(
    host,
    path=None,
    git_exe=".pixi/bin/git",
):
    """Sync the current git repo to a remote machine.

    Note: this will erase any changes on the remote machine!

    Parameters
    ----------
    host : str
        The hostname to sync to.
    path : str, optional
        The path to sync to. Defaults to the name of the current repository.
    git_exe : str, optional
        The path to the git executable on the remote machine. Defaults to `.pixi/bin/git`.
    """
    repo = git.Repo(".", search_parent_directories=True)
    if path is None:
        path = Path(repo.working_tree_dir).name

    async with asyncssh.connect(host, known_hosts=known_hosts()) as conn:
        # Get our current commit hash
        current_commit = repo.head.commit.hexsha
        await conn.run(
            f"mkdir -p {path} && {git_exe} init {path} && {git_exe} config -f {path}/.git/config "
            "receive.denyCurrentBranch updateInstead && {git_exe} -C {path} checkout "
        )
        # Check if it is already there and reset to it if it is
        if (
            await conn.run(
                f"{git_exe} -C {path} cat-file commit {current_commit}"
                f" && {git_exe} -C {path} reset --hard {current_commit}",
                check=False,
            )
        ).returncode == 0:
            return path

        repo.git.push("--force", f"{host}:{path}", "HEAD:master")
        # Now we can checkout the repo in the remote machine
        await conn.run(f"{git_exe} -C {path} checkout -f {current_commit}")

    return path
