import asyncio
import sys

from dask_jobqueue import PBSCluster

from traitlets.config import Application
from traitlets import Int, Unicode, Dict, Bool


class Server(Application):
    aliases = {
        "kwargs": "Server.kwargs",
        "extra_path": "Server.extra_path",
        "nodes": "Server.nodes",
        "adapt_min": "Server.adapt_min",
        "asynchronous": "Server.asynchronous",
        "port": "Server.port",
    }

    kwargs = Dict(config=True)
    extra_path = Unicode(config=True, allow_none=True)
    nodes = Int(config=True)
    asynchronous = Bool(help="run cluster asynchronously", config=True)
    adapt_min = Int(config=True, allow_none=True)
    port = Int(help="Port to use", config=True)

    async def main(self):
        scheduler_options = {"port": self.port, "dashboard_address": self.port + 1}
        try:
            env_extra = None
            if self.extra_path:
                env_extra = [f"-v PYTHONPATH={self.extra_path}"]
            cluster = await PBSCluster(
                job_extra_directives=env_extra,
                asynchronous=self.asynchronous,
                nanny=True,
                death_timeout=60,
                scheduler_options=scheduler_options,
                **self.kwargs,
            )

            if self.adapt_min:
                cluster.adapt(minimum=self.adapt_min, maximum=self.nodes)
            else:
                cluster.scale(self.nodes)

            scheduler = cluster.scheduler
            await scheduler.finished()
        except Exception:
            await cluster.close()
            sys.exit(1)

    def start(self):
        asyncio.run(self.main())


launch_server = Server.launch_instance
